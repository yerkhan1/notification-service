import logging
from flask import Flask, request, jsonify

app = Flask(__name__)

# Configure logging
logging.basicConfig(filename='notification_service.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

@app.route('/send_notification', methods=['POST'])
def send_notification():
    try:
        data = request.get_json()

        # Validate and process notification data
        recipient = data.get('recipient')
        message = data.get('message')

        if not recipient or not message:
            logging.error('Invalid notification data received.')
            return jsonify({"error": "Invalid notification data"}), 400

        # Simulate sending notification to Kafka
        kafka_success = simulate_send_to_kafka(recipient, message)

        if kafka_success:
            logging.info(f'Notification sent to {recipient} successfully.')
            return jsonify({"message": "Notification sent successfully"})
        else:
            logging.error(f'Failed to send notification to {recipient}.')
            return jsonify({"error": "Failed to send notification"}), 500

    except Exception as e:
        logging.exception('An error occurred while processing notification.')
        return jsonify({"error": "Internal server error"}), 500

def simulate_send_to_kafka(recipient, message):
    # Simulate sending notification to Kafka
    # Actual implementation would involve interaction with Kafka
    print(f"Sending notification to Kafka for {recipient}: {message}")
    return True  # Simulated success

import uuid
from flask import Flask, request, jsonify

app = Flask(__name__)

# In-memory storage for templates (replace with a database in a production environment)
templates = []

@app.route('/templates', methods=['GET'])
def get_templates():
    return jsonify({"templates": templates})

@app.route('/templates/<template_id>', methods=['GET'])
def get_template(template_id):
    template = next((t for t in templates if t['id'] == template_id), None)
    if template:
        return jsonify({"template": template})
    else:
        return jsonify({"error": "Template not found"}), 404

@app.route('/templates', methods=['POST'])
def create_template():
    data = request.get_json()

    template = {
        "created_at": data.get("created_at"),
        "id": str(uuid.uuid4()),  # Generate a unique ID
        "name": data.get("name"),
        "priority": data.get("priority"),
        "message": data.get("message"),
        "link_image": data.get("link_image"),
        "link_action": data.get("link_action")
    }

    templates.append(template)
    return jsonify({"message": "Template created successfully", "template": template})

@app.route('/templates/<template_id>', methods=['PUT'])
def update_template(template_id):
    template = next((t for t in templates if t['id'] == template_id), None)
    if template:
        data = request.get_json()

        template.update({
            "created_at": data.get("created_at"),
            "name": data.get("name"),
            "priority": data.get("priority"),
            "message": data.get("message"),
            "link_image": data.get("link_image"),
            "link_action": data.get("link_action")
        })

        return jsonify({"message": "Template updated successfully", "template": template})
    else:
        return jsonify({"error": "Template not found"}), 404

@app.route('/templates/<template_id>', methods=['DELETE'])
def delete_template(template_id):
    global templates
    templates = [t for t in templates if t['id'] != template_id]
    return jsonify({"message": "Template deleted successfully"})

if __name__ == '__main__':
    app.run(debug=True)
