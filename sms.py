# 1st variant Twilio
from twilio.rest import Client


# Your Twilio account SID and Auth Token
account_sid = 'your_account_sid'
auth_token = 'your_auth_token'

client = Client(account_sid, auth_token)

message = client.messages.create(
    body='Hello from Twilio!',
    from_='your_twilio_number',
    to='recipient_number'
)

print(message.sid)

# 2nd variant: using TextBelt
import requests


def send_textbelt_sms(phone, msg, apikey):
    result = True
    json_success = False

    try:
        resp = requests.post('https://textbelt.com/text', {
            'phone': phone,
            'message': msg,
            'key': apikey,
        })
    except:
        result = False

    if result:
        try:
            json_success = resp.json()["success"]
        except:
            result = False

    if result:
        if not json_success:
            result = False

    return result


phone = 'recipient_number'
smsmsg = 'Hello from Textbelt!'
apikey = 'textbelt'

if send_textbelt_sms(phone, smsmsg, apikey):
    print('SMS message successfully sent!')
else:
    print('Could not send SMS message.')
